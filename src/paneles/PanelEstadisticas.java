/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paneles;

import hielofino.Imagenes;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelEstadisticas extends JPanel {

    private static PanelEstadisticas panel;
    private int puntajeFinal;
    private String nombre;
    private int nivelesRes;
    private Timer temp;
    private int k; //representa el momento cuando aparecera cada estadistica
    private final ActionListener cambiar = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            k++;
            repaint();
        }
    };

    public PanelEstadisticas() {
        panel = null;
        puntajeFinal = 0;
        nombre = "";
        nivelesRes = 0;
        k = 0;
        temp = new Timer(1500, cambiar);
        temp.start();
    }

    public void setPuntajeFinal(int puntajeFinal, String nombre, int nivelesRes) {
        this.puntajeFinal = puntajeFinal;
        this.nombre = nombre;
        this.nivelesRes = nivelesRes;
    }

    public static PanelEstadisticas getInstance() {
        if (panel == null) {
            panel = new PanelEstadisticas();
        }
        return panel;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoEstadisticas).getImage(), 0, 0, null);
        g.setFont(new Font("Estadisticas", Font.BOLD, 25));
        g.setColor(Color.white);
        if (k >= 1) {
            g.drawString(""+nombre, 425, 203);
        }
        if (k >= 2) {
            g.drawString(""+nivelesRes, 538, 328);
        }
        if (k >= 3) {
            g.drawString(""+puntajeFinal, 421, 450);
            temp.stop();
            k = 0;
        }
    }
}
