
package paneles;

import hielofino.Imagenes;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelCreditos extends JPanel{
    private static PanelCreditos panel;
    public PanelCreditos(){
    }
    
    public static PanelCreditos getInstance(){
        if (panel == null) {
            panel = new PanelCreditos();
        }
        return panel;
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoCreditos).getImage(), 0, 0, null);
    }
}
