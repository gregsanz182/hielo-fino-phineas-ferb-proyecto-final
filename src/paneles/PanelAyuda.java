
package paneles;

import hielofino.Imagenes;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelAyuda extends JPanel{
    private static PanelAyuda panel;
    public PanelAyuda(){
    }
    
    public static PanelAyuda getInstance(){
        if (panel == null) {
            panel = new PanelAyuda();
        }
        return panel;
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoAyuda).getImage(), 0, 0, null);
    }
}
