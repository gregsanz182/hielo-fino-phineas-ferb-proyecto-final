/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paneles;

import hielofino.Imagenes;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelMenu extends JPanel{
    private static PanelMenu panel;
    
    
    public PanelMenu(){
        panel = null;
    }
    
    public static PanelMenu getInstance(){
        if (panel == null) {
            panel = new PanelMenu();
        }
        return panel;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); 
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoPrincipal).getImage(), 0, 0, null);
    }
}
