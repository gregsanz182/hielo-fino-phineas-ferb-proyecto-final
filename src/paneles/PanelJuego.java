/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paneles;

import hielofino.Imagenes;
import hielofino.Juego;
import hielofino.Nivel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelJuego extends JPanel {

    private static PanelJuego panel;
    private Font fuenteJuego;
    private Timer temp;
    private ActionListener action = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            repaint();
        }
    };
    private int k;

    public Timer getTemp() {
        return temp;
    }

    public PanelJuego() {
        setPreferredSize(new Dimension(900, 600));
        k = 0;
        temp = new Timer(100, action);
        temp.start();
    }

    public static PanelJuego getInstance() {
        if (panel == null) {
            panel = new PanelJuego();
        }
        return panel;
    }

    @Override
    public void paint(Graphics g) {
        fuenteJuego = new Font("Juego", Font.BOLD, 25);
        super.paint(g);
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoJuego).getImage(), 0, 0, null);
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 19; j++) {
                if (Nivel.getInstance().getTipo(Nivel.getInstance().getNivelActual(), i, j) == Imagenes.caida) { //pinta solo la caida
                    g.drawImage(Imagenes.getInstance().getCaida(k++).getImage(), 122 + (j * 28), 142 + (i * 27), null);
                    if (k >= 5) {
                        k = 0;
                    }
                } else {
                    g.drawImage(Imagenes.getInstance().getImagen(Nivel.getInstance().getTipo(Nivel.getInstance().getNivelActual(), i, j)).getImage(), 122 + (j * 28), 142 + (i * 27), null);
                }//pinta cada uno de los objetos
            }
        }
        g.setFont(new Font("Juego", Font.BOLD, 25)); //cambia la fuente a 25 negrita
        g.setColor(Color.white); //cambia la fuente a color blanco
        g.drawString("" + Juego.getInstance().getUsuarioActual().getPuntaje(), 790, 560); //pinta el puntaje
        g.setFont(new Font("Demas", Font.BOLD, 21));
        g.drawString(Juego.getInstance().getUsuarioActual().getNombre(), 244, 110); //pinta el nombre
        g.drawString("" + (Nivel.getInstance().getNivelActual() + 1), 835, 161);
        g.drawString(Nivel.getInstance().getCantBolquesElim() + "/" + Nivel.getInstance().getCantBloques(), 802, 208);
        g.drawString(Juego.getInstance().getUsuarioActual().getNivelesRes() + "/19", 802, 258);

    }
}
