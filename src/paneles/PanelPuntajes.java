/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package paneles;

import hielofino.Imagenes;
import hielofino.Juego;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class PanelPuntajes extends JPanel {

    private static PanelPuntajes panel;

    public PanelPuntajes() {
    }

    public static PanelPuntajes getInstance() {
        if (panel == null) {
            panel = new PanelPuntajes();
        }
        return panel;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(Imagenes.getInstance().getImagen(Imagenes.fondoPuntajes).getImage(), 0, 0, null);
        g.setFont(new Font("Estadisticas", Font.BOLD, 25));
        g.setColor(Color.white);
        if (Juego.getInstance().getUsuarios().size() <= 10) {
            for (int i = 0; i < Juego.getInstance().getUsuarios().size(); i++) {
                g.drawString(Juego.getInstance().getUsuarios().get(i).getNombre(), 370, 160 + (i * 35));
                g.drawString("" + Juego.getInstance().getUsuarios().get(i).getPuntaje(), 500, 160 + (i * 35));
            }
            return;
        }
        for (int i = 0; i < 10; i++) {
            g.drawString(Juego.getInstance().getUsuarios().get(i).getNombre(), 370, 160 + (i * 35));
            g.drawString("" + Juego.getInstance().getUsuarios().get(i).getPuntaje(), 500, 160 + (i * 35));
        }
    }
}
