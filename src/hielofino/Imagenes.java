package hielofino;

import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Imagenes {

    private static Imagenes imagen;
    /**
     * Enumeracion de imagenes del juego
     */
    public static final int bg = 0;
    public static final int pared = 1;
    public static final int hielo = 2;
    public static final int hieloEsp = 3;
    public static final int agua = 4;
    public static final int puertaLlave = 5;
    public static final int puerta = 6;
    public static final int portal1 = 7;
    public static final int portal2 = 8;
    public static final int tesoro = 9;
    public static final int llave = 10;
    public static final int bloque = 11;
    public static final int espacio = 12;
    public static final int player = 13;
    public static final int caida = 14;
    /**
     * Enumeracion de fondos
     */
    public static final int fondoPrincipal = 15;
    public static final int fondoJuego = 16;
    public static final int fondoEstadisticas = 17;
    public static final int fondoPuntajes = 18;
    public static final int fondoAyuda = 19;
    public static final int fondoCreditos = 20;
    /**
     * Enumeracion de iconos
     */
    public static final int icono = 21;
    public static final int guardar = 22;
    public static final int cargar = 23;
    public static final int aviso = 24;
    /**
     * Vectores de las imagenes
     */
    private ImageIcon imagenes[]; // vector donde se guardaran todas las imagenes a utilizar
    private ImageIcon caidaAnimacion[];

    public Imagenes() {
        String[] dir = new String[]{"bg_1.png", "Pared_1.png", "Hielo_1.png",
            "Hielo_2.png", "Ola_1.gif", "Puerta_2.png",
            "Puerta_1.png", "Portal_1.png", "Portal_2.png", "Tesoro_1.png",
            "Llave_1.gif", "Bloque_1.png", "Espacio_1.png",
            "Player_1.png", "Candace_1.gif", "Background_1.png",
            "Background_2.png", "Background_3.png", "Background_4.png", "Background_5.png", "Background_6.png", "Icono.png",
            "Guardar_1.png", "Cargar_1.png", "Warning_1.png"};
        imagenes = new ImageIcon[dir.length];
        URL direct;
        for (int i = 0; i < dir.length; i++) {
            direct = this.getClass().getResource("/imagenes/" + dir[i]);
            imagenes[i] = new ImageIcon(direct);
        }
        caidaAnimacion = new ImageIcon[5];
        for (int i = 0; i < caidaAnimacion.length; i++) {
            direct = this.getClass().getResource("/imagenes/Caida_" + (i + 1) + ".png");
            caidaAnimacion[i] = new ImageIcon(direct);
        }
    }

    /**
     * Se obtiene el valor que se encuentra en el vector de imagenes para la animacion de la caida del
     * muñeco al agua.
     *
     * @param i Es la posicion que se desea obtener del vector.
     * @return caidaAnimacion[i]
     */
    public ImageIcon getCaida(int i) {
        return caidaAnimacion[i];
    }

    /**
     * Devuelve un objeto de la clase Imagenes.
     *
     * @return imagen
     */
    public static Imagenes getInstance() {
        if (imagen == null) {
            imagen = new Imagenes();
        }
        return imagen;
    }

    /**
     * Se obtiene el valor del vector que se encuentra en el vector de imagenes del juego,
     * en este se encuentran la mayoria de imagenes del juego
     * @param i Es la posicion que se desea obtener el vector.
     * @return imagenes[i]
     */
    public ImageIcon getImagen(int i) {
        return imagenes[i];
    }
}
