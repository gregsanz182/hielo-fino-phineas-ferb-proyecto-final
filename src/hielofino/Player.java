package hielofino;

import java.awt.Point;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Player {

    private boolean especial; //para saber si el jugador esta sobre un hielo especial
    private boolean llave; //para saber si el jugador tiene la llave
    private Point pos; //posicion actual del jugador
    private static Player jugador;
    private boolean espacio; //para saber si el jugador esta sobre donde solia estar un portal
    private boolean puerta;

    public Player() {
        jugador = null;
        pos = new Point(0, 0);
        llave = false;
        espacio = false;
        especial = false;
        puerta = false;
    }

    /**
     * Devuelve true si el jugador ya paso sobre la puerta
     *
     * @return puerta
     */
    public boolean isPuerta() {
        return puerta;
    }

    /**
     * Cambia el valor de la puerta, true o false.
     *
     * @param puerta
     */
    public void setPuerta(boolean puerta) {
        this.puerta = puerta;
    }

    /**
     *
     * @return espacio
     */
    public boolean isEspacio() {
        return espacio;
    }

    /**
     *
     * @param espacio
     */
    public void setEspacio(boolean espacio) {
        this.espacio = espacio;
    }

    /**
     * Devuelve si esta sobre un cubo de hielo especial
     *
     * @return
     */
    public boolean isEspecial() {
        return especial;
    }

    /**
     * Cambia el valor a true o false si esta o no en un cubo especial.
     *
     * @param especial
     */
    public void setEspecial(boolean especial) {
        this.especial = especial;
    }

    /**
     * Devuelve true o false si tiene la llave.
     *
     * @return llave
     */
    public boolean isLlave() {
        return llave;
    }

    /**
     * Cambia el valor de llave, segun sea el caso si la tiene o no.
     *
     * @param llave
     */
    public void setLlave(boolean llave) {
        this.llave = llave;
    }

    /**
     * Devuelve la posicion del jugador.
     *
     * @return pos Point.
     */
    public Point getPos() {
        return pos;
    }

    /**
     * Cambia la posicion del jugador.
     *
     * @param pos
     */
    public void setPos(Point pos) {
        this.pos = pos;
    }

    /**
     * Devuelve la instancia de la clase Player
     *
     * @return jugador
     */
    public static Player getInstance() {
        if (jugador == null) {
            jugador = new Player();
        }
        return jugador;
    }
}
