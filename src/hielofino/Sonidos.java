/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hielofino;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Sonidos {
    /**
     * Enumeracion de sonidos del juego
     */
    private static Sonidos sonido;
    public static final int caminar = 0;
    public static final int hieloEspecial = 1;
    public static final int siguienteNivel = 2;
    public static final int caida = 3;
    public static final int portal = 4;
    public static final int llave = 5;
    public static final int bonus = 6;
    public static final int bloque = 7;
    private Clip[] sonidos; //vector donde se guardaran todos los sonidos

    public Sonidos() {
        String[] dir = new String[]{"caminar_hielo.wav", "salida_hielo_especial.wav", "siguiente_nivel.wav",
            "caida.wav", "portal.wav", "llave.wav", "bonus.wav", "bloque.wav"};
        sonidos = new Clip[dir.length];
        URL direct;
        for (int i = 0; i < dir.length; i++) {
            try {
                direct = this.getClass().getResource("/sonidos/" + dir[i]);
                sonidos[i] = AudioSystem.getClip();
                sonidos[i].open(AudioSystem.getAudioInputStream(direct));
            } catch (LineUnavailableException ex) {
                Logger.getLogger(Sonidos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Sonidos.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedAudioFileException ex) {
                Logger.getLogger(Sonidos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    /**
     * reproduce el sonido que representa la acción
     * * @param numClip Representa la posicion en el vector donde esta el sonido que se desea reproducir
     *
     */
    public void reproducirSonido(int numClip) {
        if (!sonidos[llave].isRunning() && !sonidos[portal].isRunning() && !sonidos[bonus].isRunning()) {
            sonidos[numClip].stop();
            sonidos[numClip].setFramePosition(0);
            sonidos[numClip].start();
        }
    }
    /**
     * Devuelve un objeto de la clase Sonidos.
     *
     * @return sonido
     */

    public static Sonidos getInstance() {
        if (sonido == null) {
            sonido = new Sonidos();
        }
        return sonido;
    }
}
