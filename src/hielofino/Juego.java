package hielofino;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import paneles.*;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Juego extends JFrame {

    private JPanel panel;
    private static Juego game;
    public static final int ancho = 900;
    public static final int alto = 600;
    private Usuario usuarioActual;
    private ArrayList<Usuario> usuarios;
    private int pos;
    private BufferedWriter out;
    private BufferedReader in;
    private boolean bloqueado;

    public Juego() {
        bloqueado = false;
        game = null;
        usuarioActual = null;
        usuarios = new ArrayList<Usuario>();
        cargarUsuarios();
        initComponents();
        definirEvents();
    }

    /**
     * Este metodo inicializa los valore y componentes que va a tener el Frame,
     * que es la ventana principal
     */
    private void initComponents() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); //No cierra la ventana por defecto
        setTitle("Hielo Fino - Phineas y Ferb"); //Titulo de la ventana
        setSize(900, 600); //Tamaño para la ventana
        setLocationRelativeTo(null); //Centra la ventana en la pantalla
        setUndecorated(true); //Quita todos los botones y decoraciones por defecto del frame
        setResizable(false); //No permite cambiar el tamaño de la ventana
        setIconImage(Imagenes.getInstance().getImagen(Imagenes.icono).getImage());
        panel = PanelMenu.getInstance(); //Panel Principal
        getContentPane().add(PanelMenu.getInstance()); //Se agrega el panel principal (o menú) al frame
    }

    /**
     * Define y añade los eventos del juego, como lo son el KeyEvents y el
     * MouseListener a cada uno de los componentes que lo necesitan.
     */
    private void definirEvents() {
        addKeyListener(new KeyEvents()); //Solo para el panel de juego
        PanelMenu.getInstance().addMouseListener(new MouseMenu()); //Listener para los evetos del mouse en el Panel del menu
        PanelJuego.getInstance().addMouseListener(new MouseMenu());
        PanelEstadisticas.getInstance().addMouseListener(new MouseMenu());
        PanelPuntajes.getInstance().addMouseListener(new MouseMenu());
        PanelAyuda.getInstance().addMouseListener(new MouseMenu());
        PanelCreditos.getInstance().addMouseListener(new MouseMenu());
    }
//
//    public boolean isBloqueado() {
//        return bloqueado;
//    }

    /**
     * Cambia el estado de la variable bloqueado, que se utiliza para bloquear
     * las acciones del teclado en el juego
     *
     * @param bloqueado
     */
    public void setBloqueado(boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    /**
     * Devuelve un objeto de la clase Juego
     *
     * @return game
     */
    public static Juego getInstance() {
        if (game == null) {
            game = new Juego(); //si no se ha creado, se crea
        }
        return game; //una vez creada se retorna
    }

    /**
     * Cambia de panel segun la instancia del objeto panel.
     *
     * @param pan
     */
    public void cambiarPanel(int pan) {
        panel.setVisible(false);
        if (panel instanceof PanelMenu) {
            getContentPane().remove(PanelMenu.getInstance()); //si el panel es PanelMenu, se borra este del Frame
        }
        if (panel instanceof PanelJuego) {
            getContentPane().remove(PanelJuego.getInstance()); //si el panel es PanelJuego, se borra este del Frame
        }
        if (panel instanceof PanelAyuda) {
            getContentPane().remove(PanelAyuda.getInstance()); //si el panel es PanelAyuda, se borra este del Frame
        }
        if (panel instanceof PanelEstadisticas) {
            getContentPane().remove(PanelEstadisticas.getInstance()); //si el panel es PanelEstadisticas, se borra este del Frame
        }
        if (panel instanceof PanelPuntajes) {
            getContentPane().remove(PanelPuntajes.getInstance()); //si el panel es PanelEstadisticas, se borra este del Frame
        }
        if (panel instanceof PanelCreditos) {
            getContentPane().remove(PanelCreditos.getInstance()); //si el panel es PanelCreditos, se borra este del Frame
        }
        switch (pan) {
            case Imagenes.fondoJuego: //numero que representa el panel de juego
                panel = PanelJuego.getInstance(); //el panel actual ahora sera Panel de juego
                getContentPane().add(PanelJuego.getInstance()); //se agrega el panel de juego al Frame
                break;
            case Imagenes.fondoPrincipal:
                panel = PanelMenu.getInstance();
                getContentPane().add(PanelMenu.getInstance());
                break;
            case Imagenes.fondoAyuda:
                panel = PanelAyuda.getInstance();
                getContentPane().add(PanelAyuda.getInstance());
                break;
            case Imagenes.fondoEstadisticas:
                panel = PanelEstadisticas.getInstance();
                getContentPane().add(PanelEstadisticas.getInstance());
                break;
            case Imagenes.fondoPuntajes:
                panel = PanelPuntajes.getInstance();
                getContentPane().add(PanelPuntajes.getInstance());
                break;
            case Imagenes.fondoCreditos:
                panel = PanelCreditos.getInstance();
                getContentPane().add(PanelCreditos.getInstance());
                break;
        }
        panel.setVisible(true);
    }

    /**
     * Se obtiene el usuario actual que esta jugando.
     *
     * @return usuarioActual
     */
    public Usuario getUsuarioActual() {
        return usuarioActual;
    }

    /**
     * Crea un nuevo usuario cuando se le da al boton Play del panel principal
     *
     * @return user El nombre del usuario
     */
    private String crearUsuario() {
        int aux;
        String user;
        user = "";
        do {
            aux = 1;
            user = (String) JOptionPane.showInputDialog(this, "Ingrese Usuario (min 3, max 6 letras)", "Iniciar Seccion", JOptionPane.INFORMATION_MESSAGE);

            if (user == null) {
                return user;
            }
            for (int i = 0; i < 10; i++) {
                if (user.contains(String.valueOf(i))) {
                    aux = 0;
                }
            }
            if (aux == 0) {
                JOptionPane.showMessageDialog(this, "Disculpe no puede ingresar numeros", "Advertencia", JOptionPane.OK_OPTION, Imagenes.getInstance().getImagen(Imagenes.aviso));
            }
        } while (aux == 0 || user.length() > 7 || user.length() < 3 || user.contains(" "));
        pos = buscarUsuario(user);
        usuarioActual = new Usuario(user, 0);

        return user;
    }

    /**
     * Carga una partida guardada, en caso de que este.
     */
    private void cargarPartida() {
        try {
            File fr = new File("Partidas.txt");
            if (fr.exists()) {
                int opc = JOptionPane.showConfirmDialog(this, "Desea abrir la partida guardada?", "Partida Guardada", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, Imagenes.getInstance().getImagen(Imagenes.cargar));
                switch (opc) {
                    case JOptionPane.YES_OPTION:
                        in = new BufferedReader(new FileReader(fr));
                        try {
                            String user = in.readLine();
                            if (user != null) {
                                int puntaje = Integer.parseInt(in.readLine());
                                int nr = Integer.parseInt(in.readLine());
                                pos = buscarUsuario(user);
                                usuarioActual = new Usuario(user, puntaje);
                                usuarioActual.initNivelesRes(nr);

                                guardarUsuario();

                                Nivel.getInstance().setNivelActual(Integer.parseInt(in.readLine()));
                                String linea;

                                for (int k = 0; k < Nivel.getInstance().getnNiveles(); k++) {
                                    for (int i = 0; i < Nivel.filas; i++) {
                                        linea = in.readLine();
                                        String tokens[] = linea.split(" ");
                                        for (int j = 0; j < Nivel.columnas; j++) {
                                            Nivel.getInstance().setTipo(k, i, j, Integer.parseInt(tokens[j]));
                                        }
                                    }
                                }
                                Nivel.getInstance().positionPlayer();
                                Nivel.getInstance().setMatriz();
                                cambiarPanel(Imagenes.fondoJuego);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        try {
                            in.close();
                        } catch (IOException ex) {
                            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    case JOptionPane.NO_OPTION:
                        cambiarPanel(Imagenes.fondoPrincipal);
                        break;
                    case JOptionPane.CANCEL_OPTION:
                        cambiarPanel(Imagenes.fondoPrincipal);
                        break;
                    case JOptionPane.CLOSED_OPTION:
                        cambiarPanel(Imagenes.fondoPrincipal);
                        break;
                }
            } else {
                JOptionPane.showMessageDialog(this, "No hay partidas guardadas", "Advertencia", JOptionPane.OK_OPTION, Imagenes.getInstance().getImagen(Imagenes.aviso));
                cambiarPanel(Imagenes.fondoPrincipal);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Guarda la partida en un archivo, para posteriormente leerla y seguir
     * jugando donde quedo.
     */
    private void guardarPartida() {
        try {
            out = new BufferedWriter(new FileWriter("Partidas.txt", false));
            out.write(usuarioActual.getNombre());
            out.newLine();
            out.write("" + usuarioActual.getPuntaje());
            out.newLine();
            out.write("" + usuarioActual.getNivelesRes());
            out.newLine();
            out.write("" + Nivel.getInstance().getNivelActual());
            out.newLine();
            for (int k = 0; k < Nivel.getInstance().getnNiveles(); k++) {
                for (int i = 0; i < Nivel.filas; i++) {
                    for (int j = 0; j < Nivel.columnas; j++) {
                        out.write("" + Nivel.getInstance().getTipo(k, i, j));
                        if (j < (Nivel.columnas - 1)) {
                            out.write(" ");
                        }
                    }
                    out.newLine();
                }
            }
            out.flush();
            out.close();


        } catch (IOException ex) {
            Logger.getLogger(Usuario.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * guarda el usuario actual en un vector de usuarios de tipo Ususario que
     * almacena los atributos de la misma clase.
     */
    private void guardarUsuario() {
        if (pos == -1) {
            usuarios.add(usuarioActual);
        } else if (usuarios.get(pos).getPuntaje() < usuarioActual.getPuntaje()) {
            usuarios.set(pos, usuarioActual);
        }
        ordenarUsuarios();
    }

    /**
     * Busca en el vector de usuarios segun el nombre que le envian, si lo
     * encuentra retorna la posicion donde se encuetra, en caso contrario
     * retorna -1.
     *
     * @param user
     * @return i, -1.
     */
    private int buscarUsuario(String user) {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i).getNombre().equals(user)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Ordena los usuarios de puntaje mayor a menor.
     */
    private void ordenarUsuarios() {
        Usuario aux;
        for (int i = 1; i < usuarios.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (usuarios.get(i).getPuntaje() > usuarios.get(j).getPuntaje()) {
                    aux = usuarios.get(j);
                    usuarios.set(j, usuarios.get(i));
                    usuarios.set(i, aux);
                }
            }
        }
    }

    public class MouseMenu extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            Point click = new Point(e.getX(), e.getY()); //Se guarda la posicion en X y Y del cursor
            comprobarBotones(click); //Se llama al metodo que comprobara se en la posicion X y Y se encuentra algun boton
        }
    }

    public class KeyEvents extends KeyAdapter {

        private int dy; //variable que contendra hacia donde de movera el jugador en y
        private int dx; //variable que contendra hacia donde de movera el jugador en X

        @Override
        public void keyPressed(KeyEvent e) {
            if (panel instanceof PanelJuego && !bloqueado) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_UP: //si se presiona la tecla direccional hacia arriba
                        dy = -1;
                        dx = 0;
                        break;
                    case KeyEvent.VK_DOWN: //si se presiona la tecla direccional hacia abajo
                        dy = 1;
                        dx = 0;
                        break;
                    case KeyEvent.VK_LEFT: //si se presiona la tecla direccional hacia la izquierda
                        dx = -1;
                        dy = 0;
                        break;
                    case KeyEvent.VK_RIGHT: //si se presiona la tecla direccional hacia la derecha
                        dx = 1;
                        dy = 0;
                        break;
                }
                Nivel.getInstance().validar(dx, dy); //se llama al metodo que validara si se puede mover el jugador hacia esas direcciones

                repaint(); //se pinta de nuevo el frame
            }

        }
    }

    /**
     * Botones de los paneles
     *
     * @param c de tipo Point cordenadas para la ubicacion del mouse cuando hace
     * click.
     */
    public void comprobarBotones(Point c) {
        int opc;
        if (panel instanceof PanelMenu) {
            if (c.x >= 590 && c.x <= 734 && c.y >= 450 && c.y <= 558) { //si encuentra el boton play para comenzar el juego
                if (crearUsuario() != null) {
                    cambiarPanel(Imagenes.fondoJuego); //fondoJuego representa al panel menu
                }
            }
            if (c.x >= 12 && c.x <= 118 && c.y >= 397 && c.y <= 475) {
                cambiarPanel(Imagenes.fondoAyuda);
            }
            if (c.x >= 674 && c.x <= 780 && c.y >= 321 && c.y <= 399) {
                cargarPartida();
            }
            if (c.x >= 332 && c.x <= 438 && c.y >= 377 && c.y <= 455) {
                cambiarPanel(Imagenes.fondoPuntajes);
            }
            if (c.x >= 785 && c.x <= 894 && c.y >= 561 && c.y <= 594) {
                cambiarPanel(Imagenes.fondoCreditos);
            }
            if (c.x >= 855 && c.x <= 883 && c.y >= 15 && c.y <= 44) {
                almacenarUsuarios();
                System.exit(0);
            }
        }
        if (panel instanceof PanelJuego) {
            if (c.x >= 21 && c.x <= 54 && c.y >= 18 && c.y <= 55) {
                guardarUsuario();
                opc = JOptionPane.showConfirmDialog(this, "Desea Guardar la partida", "Guardar Partida", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, Imagenes.getInstance().getImagen(Imagenes.guardar));
                switch (opc) {
                    case JOptionPane.YES_OPTION:
                        guardarPartida();
                        cambiarPanel(Imagenes.fondoPrincipal);
                        Nivel.getInstance().Inicializar();
                        break;
                    case JOptionPane.NO_OPTION:
                        cambiarPanel(Imagenes.fondoPrincipal);
                        Nivel.getInstance().Inicializar();
                        break;
                    case JOptionPane.CANCEL_OPTION:

                        break;
                    case JOptionPane.CLOSED_OPTION:

                        break;
                }
            }
            if (c.x >= 542 && c.x <= 660 && c.y >= 93 && c.y <= 120) {
                Nivel.getInstance().resetNivel();
            }
        }
        if (panel instanceof PanelEstadisticas) {
            if (c.x >= 21 && c.x <= 54 && c.y >= 18 && c.y <= 55) {
                cambiarPanel(Imagenes.fondoPrincipal);
            }
        }
        if (panel instanceof PanelPuntajes) {
            if (c.x >= 21 && c.x <= 54 && c.y >= 18 && c.y <= 55) {
                cambiarPanel(Imagenes.fondoPrincipal);
            }
        }
        if (panel instanceof PanelAyuda) {
            if (c.x >= 21 && c.x <= 54 && c.y >= 18 && c.y <= 55) {
                cambiarPanel(Imagenes.fondoPrincipal);
            }
        }
        if (panel instanceof PanelCreditos) {
            if (c.x >= 21 && c.x <= 54 && c.y >= 18 && c.y <= 55) {
                cambiarPanel(Imagenes.fondoPrincipal);
            }
        }
    }

    /**
     * Se obtiene el ArrayList de los usuarios.
     *
     * @return usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * Guarda en un archivo los usuarios que estan en el ArrayList de mayor a
     * menor puntaje, cuando se cierra el juego.
     */
    public void almacenarUsuarios() {
        try {
            if (usuarios.size() >= 1) {
                out = new BufferedWriter(new FileWriter("informacion.txt"));
                if (usuarios.size() <= 10) {
                    for (int i = 0; i < usuarios.size(); i++) {
                        out.write(usuarios.get(i).getNombre());
                        out.newLine();
                        out.write("" + usuarios.get(i).getPuntaje());
                        if (i < (usuarios.size() - 1)) {
                            out.newLine();
                        }
                    }
                } else {
                    for (int i = 0; i < 10; i++) {
                        out.write(usuarios.get(i).getNombre());
                        out.newLine();
                        out.write("" + usuarios.get(i).getPuntaje());
                        if (i < (6 - 1)) {
                            out.newLine();
                        }
                    }
                }
                out.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Carga desde un archivo al ArrayList de Usuarios los usuarios almacenados
     * con su respectivo puntaje.
     */
    private void cargarUsuarios() {
        File file = new File("informacion.txt");
        if (file.exists()) {
            try {
                in = new BufferedReader(new FileReader(file));
                try {
                    String user = in.readLine();
                    while (user != null) {
                        pos = buscarUsuario(user);
                        int puntaje = Integer.parseInt(in.readLine());
                        usuarioActual = new Usuario(user, puntaje);
                        guardarUsuario();
                        user = in.readLine();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
