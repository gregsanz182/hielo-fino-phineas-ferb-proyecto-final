package hielofino;

import static hielofino.Nivel.nombreArchivo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.Timer;
import paneles.PanelJuego;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Nivel {
    
    private int[][] matriz;
    private int[][][] niveles;
    private static Nivel nivel;
    public static final int filas = 15;
    public static final int columnas = 19;
    public static final String nombreArchivo = "niveles.txt";
    private int nNiveles;
    private int nivelActual;
    private BufferedReader in;
    private int cantBloques;
    private int cantBolquesElim;
    private int auxPuntaje;
    private int dx;
    private int dy;
    private Timer timerCaida;
    private Timer timerBloque;
    private final ActionListener accionBloque = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            moverBloque();
        }
    };
    private final ActionListener accionCaida = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            resetNivel();
            Juego.getInstance().setBloqueado(false);
            timerCaida.stop();
            
        }
    };
    private boolean especial;
    private int contX;
    private int contY;
    
    public Nivel() {
        in = null;
        nivelActual = 0;
        cantBloques = 0;
        cantBolquesElim = 0;
        dx = 0;
        dy = 0;
        nNiveles = 0;
        auxPuntaje = 0;
        nivel = null;
        timerCaida = new Timer(500, accionCaida);
        timerBloque = new Timer(100, accionBloque);
        especial = false;
        contX = 0;
        contY = 0;
        cargarNiveles();
        positionPlayer();
        CantBloques();
        setMatriz();
    }

    /**
     * Devulve el nivel actual donde se esta jugando.
     *
     * @return
     */
    public int getNivelActual() {
        return nivelActual;
    }

    /**
     * Devuelve la cantidad de bloques eliminados en el nivel actual.
     *
     * @return cantBloquesElim.
     */
    public int getCantBolquesElim() {
        return cantBolquesElim;
    }

    /**
     * Cuenta la cantidad de bloques que puede derretir el jugador en cada
     * nivel.
     */
    private void CantBloques() {
        cantBloques = 0;
        for (int j = 0; j < filas; j++) {
            for (int k = 0; k < columnas; k++) {
                if (niveles[nivelActual][j][k] == Imagenes.hielo) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.hieloEsp) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.llave) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.tesoro) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.puertaLlave) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.puerta) {
                    cantBloques++;
                }
                if (niveles[nivelActual][j][k] == Imagenes.bloque) {
                    cantBloques++;
                }
            }
        }
    }

    /**
     * Devuelve la cantidad de bloques del nivel actual.
     *
     * @return cantBloques
     */
    public int getCantBloques() {
        return cantBloques;
    }

    /**
     * Carga desde un archivo los niveles del juego y los guarda en un vector de
     * matrices llamado niveles.
     */
    private void cargarNiveles() {
        try {
            in = new BufferedReader(new FileReader(nombreArchivo));
            
            nNiveles = Integer.parseInt(in.readLine());
            niveles = new int[nNiveles][filas][columnas];
            matriz = new int[filas][columnas];
            String linea;
            
            
            for (int i = 0; i < nNiveles; i++) {
                for (int j = 0; j < filas; j++) {
                    linea = in.readLine();
                    String tokens[] = linea.split(" ");
                    for (int k = 0; k < columnas; k++) {
                        niveles[i][j][k] = Integer.parseInt(tokens[k]);
                    }
                }
            }
            in.close();
        } catch (IOException e) {
            System.out.println("ERROR AL ABRIR EL ARCHIVO...");
        }
    }

    /**
     * Posiciona el jugador en el nivel actual.
     */
    public void positionPlayer() {
        Point pos;
        for (int j = 0; j < filas; j++) {
            for (int k = 0; k < columnas; k++) {
                if (niveles[nivelActual][j][k] == Imagenes.player) {
                    pos = new Point(k, j);
                    Player.getInstance().setPos(pos);
                }
            }
        }
    }

    /**
     * Cambia de nivel cuando el jugador llega a la puerta.
     */
    private void cambiarNivel() {
        nivelesRes();
        nivelActual++;
        cantBolquesElim = 0;
        auxPuntaje = 0;
        if (nivelActual >= nNiveles) {
            Juego.getInstance().cambiarPanel(Imagenes.fondoEstadisticas);
            paneles.PanelEstadisticas.getInstance().setPuntajeFinal(Juego.getInstance().getUsuarioActual().getPuntaje(), Juego.getInstance().getUsuarioActual().getNombre(), Juego.getInstance().getUsuarioActual().getNivelesRes());
            Inicializar();
        } else {
            positionPlayer();
            CantBloques();
            setMatriz();
        }
    }
//
//    public void setAuxPuntaje(int auxPuntaje) {
//        this.auxPuntaje = auxPuntaje;
//    }

    /**
     * Inicializa los niveles cuando se termina de jugar.
     */
    public void Inicializar() {
        Player.getInstance().setLlave(false);
        nivel = null;
    }

    /**
     * Devuelve el valor que se encuentra en el vector de matrices.
     *
     * @param k = nivel actual
     * @param i = filas
     * @param j = columnas
     * @return niveles[k][i][j]
     */
    public int getTipo(int k, int i, int j) {
        return niveles[k][i][j]; //se envia el tipo de objeto que hay en esa posicion de la niveles[nivelActual]
    }

    /**
     * Cambia el valor del vector de matrices segun las posicione que le envian.
     *
     * @param k = nivel actual
     * @param i = filas
     * @param j = columnas
     * @param valor
     */
    public void setTipo(int k, int i, int j, int valor) {
        this.niveles[k][i][j] = valor;
    }

    /**
     * Devuelve la instancia de la clase Nivel.
     *
     * @return nivel
     */
    public static Nivel getInstance() {
        if (nivel == null) {
            nivel = new Nivel(); //se incializa en el nivel 1
        }
        return nivel;
    }

    /**
     * Translada el jugador a las posiciones que recibe.
     *
     * @param dx
     * @param dy
     */
    public void cambiarJugador(int dx, int dy) {
        Player.getInstance().getPos().translate(dx, dy); //cambia la posicion actual del jugador
        niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.player; //cambia la posicion del jugador en la niveles[nivelActual]
    }

    /**
     * Cambia a agua la posicion actual del jugador a lo que avanza.
     */
    public void cambiarAgua() {
        if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] == Imagenes.player && Player.getInstance().isEspecial() == false && Player.getInstance().isEspacio() == false) {
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.agua; //se derrite el hielo donde estaba el jugador
            cantBolquesElim++;
            Sonidos.getInstance().reproducirSonido(Sonidos.caminar);
        } else if (Player.getInstance().isEspacio() == true) {
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.portal2; //donde estaba el jugador ahora es un espacio donde solia estar el portal
            Player.getInstance().setEspacio(false); //el jugador ya no esta en el espacio del portal
        }
        if (Player.getInstance().isEspecial() == true) {
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.hielo;
        }
    }

    /**
     * Valida los moviminetos del jugador segun se mueva en filas o culmnas
     *
     * @param dx = columnas
     * @param dy = filas
     */
    public void validar(int dx, int dy) {
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.puerta) {
            cantBolquesElim++;
            Sonidos.getInstance().reproducirSonido(Sonidos.siguienteNivel);
            cambiarNivel();
            return;
        }
        if (Player.getInstance().isLlave() == true) { //si el jugador tiene la llave
            validarLlave(dx, dy); //metodo que comprueba si hay una puerta con llave alrededor del jugador
        }
        //se comprueba si el jugador encontro la llave
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.llave) {
            Sonidos.getInstance().reproducirSonido(Sonidos.llave);
            Player.getInstance().setLlave(true); //el jugador ahora tiene la llave
        }
        //si el jugador se quiere mover a una pared o a una puerta bloqueada sin tener la llave, no se movera
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.pared
                || niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.puertaLlave) {
            return;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.agua) {
            niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] = Imagenes.caida;
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.hielo;
            Juego.getInstance().setBloqueado(true);
            Sonidos.getInstance().reproducirSonido(Sonidos.caida);
            timerCaida.start();
            PanelJuego.getInstance().getTemp().restart();
            return;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.portal1) { //si se mueve a un portal
            niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] = Imagenes.portal2; //pasa a ser un espacio donde solia estar un portal
            Sonidos.getInstance().reproducirSonido(Sonidos.portal);
            cambiarAgua(); //se derrite el hielo donde estaba el jugador
            transportarJugador(); //metodo que transporta el jugador al otro portal
            validarMovimiento();
            auxPuntaje++;
            Juego.getInstance().getUsuarioActual().setPuntaje(1); //aunmenta el puntaje en 1
            Player.getInstance().setEspacio(true); //ahora el jugador esta en un espacio donde solia estar un portal, por lo tanto cuando salga de alli no se derretira dicho espacio
            return; //se acaba el movimiento y no se entra a las proximas decisiones
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.portal2) {
            cambiarAgua(); //se derrite el hielo donde estaba el jugador
            cambiarJugador(dx, dy); //se cambia la posicion actual del jugador
            validarMovimiento();
            Player.getInstance().setEspacio(true); //ahora el jugador esta en un espacio donde solia estar un poral, por lo tanto cuando salga de alli no se derretira dicho espacio
            return; //se acaba el movimiento y no se entra a las proximas decisiones
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] == Imagenes.player && Player.getInstance().isEspecial() == true) { //si el jugador sale del hielo especial
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.hielo; //el hielo especial ahora es hielo normal
            Sonidos.getInstance().reproducirSonido(Sonidos.hieloEspecial);
            Player.getInstance().setEspecial(false); //el jugador ya no esta en un hielo especial
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.hieloEsp) { //si se avanza a un hielo especial
            cambiarAgua(); //se derrite el hielo donde estaba el jugador
            cambiarJugador(dx, dy); //se cambia la posicion actual del jugador
            validarMovimiento();
            Player.getInstance().setEspecial(true); //ahora el jugador estara sobre un hielo especial
            auxPuntaje++;
            Juego.getInstance().getUsuarioActual().setPuntaje(1); //aunmenta el puntaje en 1
            return; //se acaba el movimiento y no se entra a las proximas decisiones
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.tesoro) { //si el jugador se movera a un bonus
            auxPuntaje++;
            Sonidos.getInstance().reproducirSonido(Sonidos.bonus);
            Juego.getInstance().getUsuarioActual().setPuntaje(1); //aunmenta el puntaje en 1 + el 1 que se le sumara por defecto
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.bloque
                && (niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.agua
                || niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.pared
                || niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.puertaLlave
                || niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.portal1
                || niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.portal2)) {
            return;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + dx] == Imagenes.bloque) { //si el jugador se movera a un bonus
            this.dx = dx;
            this.dy = dy;
            auxPuntaje++;
            Juego.getInstance().getUsuarioActual().setPuntaje(1); //aunmenta el puntaje en 1
            Juego.getInstance().setBloqueado(true);
            Sonidos.getInstance().reproducirSonido(Sonidos.bloque);
            timerBloque.start();
            return;
        }
        auxPuntaje++;
        Juego.getInstance().getUsuarioActual().setPuntaje(1); //aunmenta el puntaje en 1
        cambiarAgua(); //se derrite el hielo donde estaba el jugador
        cambiarJugador(dx, dy); //se cambia la posicion actual del jugador
        validarMovimiento();
    }

    /**
     * Valida los posibles movimientos del bloque.
     */
    public void moverBloque() {
        int auxX = 0;
        int auxY = 0;
        
        auxX = contX;
        auxY = contY;
        contX += dx;
        contY += dy;
        if (niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] != Imagenes.agua
                && niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] != Imagenes.pared
                && niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] != Imagenes.puertaLlave
                && niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] != Imagenes.portal1
                && niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] != Imagenes.portal2) {
            if (especial) {
                niveles[nivelActual][Player.getInstance().getPos().y + auxY][Player.getInstance().getPos().x + auxX] = Imagenes.hieloEsp;
                especial = false;
            } else {
                niveles[nivelActual][Player.getInstance().getPos().y + auxY][Player.getInstance().getPos().x + auxX] = Imagenes.hielo;
            }
            if (niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] == Imagenes.hieloEsp) {
                especial = true;
            }
            niveles[nivelActual][Player.getInstance().getPos().y + contY][Player.getInstance().getPos().x + contX] = Imagenes.bloque;
            
            
        } else {
            timerBloque.stop();
            contX = contY = 0;
            Juego.getInstance().setBloqueado(false);
            cambiarAgua(); //se derrite el hielo donde estaba el jugador
            cambiarJugador(dx, dy); //se cambia la posicion actual del jugado
        }
        
    }

    /**
     * Transporta al jugador hasta el otro portal, buscando en el nivel actual y
     * si lo encuentra el jugador se mueve hacia esa posicion
     */
    public void transportarJugador() {
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) { //se busca otro portal
                if (niveles[nivelActual][i][j] == Imagenes.portal1) { //si lo encuetra....
                    niveles[nivelActual][i][j] = Imagenes.player; //...se mueve al jugador en la niveles[nivelActual]
                    Player.getInstance().getPos().move(j, i); //...se cambia la posicion actual del jugador
                }
            }
        }
    }

    /**
     * Valida si el jugador esta encerrado para que resetee el nivel.
     *
     * @param dx
     * @param dy
     */
    public void validarMovimiento() {
        int cont = 0;
        if (niveles[nivelActual][Player.getInstance().getPos().y + 1][Player.getInstance().getPos().x] == Imagenes.agua
                || niveles[nivelActual][Player.getInstance().getPos().y + 1][Player.getInstance().getPos().x] == Imagenes.bloque
                || niveles[nivelActual][Player.getInstance().getPos().y + 1][Player.getInstance().getPos().x] == Imagenes.pared) {
            cont++;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y - 1][Player.getInstance().getPos().x] == Imagenes.agua
                || niveles[nivelActual][Player.getInstance().getPos().y - 1][Player.getInstance().getPos().x] == Imagenes.bloque
                || niveles[nivelActual][Player.getInstance().getPos().y - 1][Player.getInstance().getPos().x] == Imagenes.pared) {
            cont++;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + 1] == Imagenes.agua
                || niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + 1] == Imagenes.bloque
                || niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + 1] == Imagenes.pared) {
            cont++;
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x - 1] == Imagenes.agua
                || niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x - 1] == Imagenes.bloque
                || niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x - 1] == Imagenes.pared) {
            cont++;
        }
        if (cont == 4) {
            niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x] = Imagenes.caida;
            Juego.getInstance().setBloqueado(true);
            timerCaida.start();
            PanelJuego.getInstance().getTemp().restart();
        }
        
    }

    /**
     * Valida cada vez que se mueva el jugador, para que busque en X y Y la
     * puerta, y si la encuentra se abre. En caso contrario si el jugador no
     * posee la llave no se abrira la puerta.
     *
     * @param dx
     * @param dy
     */
    public void validarLlave(int dx, int dy) {
        if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + dx] != Imagenes.pared
                && niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x] != Imagenes.pared) {
            if (niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x] == Imagenes.puertaLlave) { //si una posicion delante hay una puerta bloqueda...
                niveles[nivelActual][Player.getInstance().getPos().y + (dy * 2)][Player.getInstance().getPos().x] = Imagenes.hielo; //....la convierte en hielo
                Player.getInstance().setLlave(false);
                Sonidos.getInstance().reproducirSonido(Sonidos.llave);
            }
            if (niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + (dx * 2)] == Imagenes.puertaLlave) { //si una posicion delante hay una puerta bloqueda...
                niveles[nivelActual][Player.getInstance().getPos().y][Player.getInstance().getPos().x + (dx * 2)] = Imagenes.hielo; //....la convierte en hielo
                Player.getInstance().setLlave(false);
                Sonidos.getInstance().reproducirSonido(Sonidos.llave);
            }
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + 1] == Imagenes.puertaLlave) {
            niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x + 1] = Imagenes.hielo;
            Player.getInstance().setLlave(false);
            Sonidos.getInstance().reproducirSonido(Sonidos.llave);
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x - 1] == Imagenes.puertaLlave) {
            niveles[nivelActual][Player.getInstance().getPos().y + dy][Player.getInstance().getPos().x - 1] = Imagenes.hielo;
            Player.getInstance().setLlave(false);
            Sonidos.getInstance().reproducirSonido(Sonidos.llave);
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y + 1][Player.getInstance().getPos().x + dx] == Imagenes.puertaLlave) {
            niveles[nivelActual][Player.getInstance().getPos().y + 1][Player.getInstance().getPos().x + dx] = Imagenes.hielo;
            Player.getInstance().setLlave(false);
            Sonidos.getInstance().reproducirSonido(Sonidos.llave);
        }
        if (niveles[nivelActual][Player.getInstance().getPos().y - 1][Player.getInstance().getPos().x + dx] == Imagenes.puertaLlave) {
            niveles[nivelActual][Player.getInstance().getPos().y - 1][Player.getInstance().getPos().x + dx] = Imagenes.hielo;
            Player.getInstance().setLlave(false);
            Sonidos.getInstance().reproducirSonido(Sonidos.llave);
        }
    }

    /**
     * Compara la cantidad de hielo derretido con la cantidad de bloques de
     * hielo que tiene el nivel actual. Si son iguales significa que derritio
     * todos los bloques de hielo y por lo tanto los niveles resueltos
     * aumentara!
     */
    private void nivelesRes() {
        if (cantBloques == cantBolquesElim) {
            Juego.getInstance().getUsuarioActual().setNivelesRes(1);
        }
    }

    /**
     * Cambia el valor del nivel actual.
     *
     * @param nivelActual
     */
    public void setNivelActual(int nivelActual) {
        this.nivelActual = nivelActual;
    }

    /**
     * Se inicializan los valores del nivel y del jugador.
     */
    public void resetNivel() {
        for (int i = 0; i < filas; i++) {
            System.arraycopy(matriz[i], 0, niveles[nivelActual][i], 0, columnas);
            positionPlayer();
            cantBolquesElim = 0;
            Player.getInstance().setLlave(false);
            Juego.getInstance().getUsuarioActual().setPuntaje(-auxPuntaje);
            auxPuntaje = 0;
            PanelJuego.getInstance().repaint();
        }
    }

    /**
     * Se obtiene el numero de niveles.
     *
     * @return nNiveles
     */
    public int getnNiveles() {
        return nNiveles;
    }

    /**
     * Copia lo que esta en el nivel actual a una matriz auxiliar. Que a su vez
     * se utiliza para cuando se quiera resetear el nivel.
     *
     */
    public void setMatriz() {
        for (int i = 0; i < filas; i++) {
            System.arraycopy(niveles[nivelActual][i], 0, matriz[i], 0, columnas);
        }
    }
}
