package hielofino;

/**
 *
 * @author Anny Chacón
 * @author Gregory Sánchez
 */
public class Usuario {

    private int puntaje; //puntaje de el jugador
    private String nombre; //nombre de el jugador
    private int nivelesRes;

    public Usuario(String user, int punt) {
        puntaje = punt;
        nombre = user;
        nivelesRes = 0;
    }

    /**
     * Devuelve la cantidad de niveles resueltos que tiene.
     *
     * @return
     */
    public int getNivelesRes() {
        return nivelesRes;
    }

    /**
     * Aumenta el valor de los niveles resueltos.
     *
     * @param nivelesRes
     */
    public void setNivelesRes(int nivelesRes) {
        this.nivelesRes += nivelesRes;
    }

    /**
     * Devuelve el puntaje del jugador.
     *
     * @return puntaje
     */
    public int getPuntaje() {
        return puntaje;
    }

    /**
     * Aumenta el puntaje del jugador.
     *
     * @param puntaje
     */
    public void setPuntaje(int puntaje) {
        this.puntaje += puntaje; //aumentar puntaje del jugador
    }

    /**
     * Inicializa el puntaje del jugador
     *
     * @param puntaje
     */
    public void initPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    /**
     * Inicializa la cantidad de niveles resueltos
     *
     * @param NR
     */
    public void initNivelesRes(int NR) {
        this.nivelesRes = NR;
    }

    /**
     * Devuelve el nombre del jugador.
     *
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre del jugador.
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
