![Logo UNET](unetLogo.png "Logo de la UNET")

Hielo Fino (Phineas y Ferb)
============================

### Proyecto Final de Programación I (Java)

Proyecto Final (Gráfico) de Programación I. Escrito en Java.

*"Hielo Fino consiste en atravesar un laberinto derritiendo todos los cubos de hielo
hasta llegar al hielo de color rojo. Cuantos más hielos se derrita más puntos se obtienen,
pero debe tenerse especial cuidado en no caer al agua. Para moverse se utiliza las fechas del
teclado."*

Proyecto desarrollado en NetBeans.

#### Enunciado

Para más información revisar el enunciado "Proyecto Juego 2012-II.pdf"

#### Desarrolladores
* [Anny Chacón (@AnnyChacon)](https://github.com/AnnyChacon)
* [Gregory Sánchez (@gregsanz182)](https://github.com/gregsanz182)

#### Asignatura
* Nombre: Programación I
* Código: 0416202T
* Profesor: Manuel Sánchez

*Proyecto desarrollado con propositos educativos para la **Universidad Nacional
Experimental del Táchira***
